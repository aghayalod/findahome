# Canvas / Discordgroup number :10 am Group 12

# Names of the team members: Anish Roy, Avi Ghayalod, Isaac Adams, Mya Mahaley, Anthony Chhang

# Name of the project: FindaHome

# URL of the GitLab repo

```
https://gitlab.com/aghayalod/findahome.git
```
# The Proposed Project

```
FindaHome is a project based around helping individuals looking for asylum or as
refugees find information about the countries they are considering moving to. This
includes social welfare information as well as general information regarding the country’s
asylum status.
```
# API URLs

```
https://api.unhcr.org/docs/index.html
```
```
https://www.globalgiving.org/api/methods/get-all-projects-for-a-country/
```
```
https://www.numbeo.com/api/doc.jsp
```
```
https://data.oecd.org/api/sdmx-json-documentation/,
https://stats.oecd.org/index.aspx?DataSetCode=BLI
```
```
https://partner.monster.com/job-search
```
```
https://documenter.getpostman.com/view/1134062/T1LJjU52
```
# Charity

Searchable Attributes:

1. Name
2. Country/Region
3. Theme
4. Address
5. Activities

Sortable Attributes:

1. Status
2. Total projects
3. Funding
4. Goal
5. Approval Date



Connections to other Models:

1. Charity -> Job: Charities provide assistance to people looking for employment
2. Country Connection: Charities provide assistance for people trying to immigrate

Rich Media:

1. Logo
2. Website URL

Instances: 400

# Jobs

Searchable Attributes:
1. Job Title
2. Job URL
3. Company Name
4. Skills
5. Charities


Filterable/Sortable Attributes:
1. Date Created
2. Job Type (Ex: PartTime, Contract, Temp)
3. Country
4. State
5. City
6. Distance 

Connections to other Models:
1. Job -> Country: Each job is located in a country
2. Job -> Charities: Based on the location of the job, One or more charities will be listed to aid with relocating and/or acquiring the job

Rich Media:
1. Map
2. Company Logo

Instances: 1000+


# Country

Searchable Attributes:

1. Country Name
2. Language
3. Region
4. Population
5. Currency

Filterable/Sortable Attributes:

1. Dwellings without basic facilities
2. Household Net worth
3. Employment Rate
4. Educational Attainment
5. Life Expectancy
6. Life Satisfaction
7. Homicide Rate
8. Employees working very long hours
9. Voter Turnout

Connections to other Models:

1. Country -> Charity: Each country has charities available
2. Country -> Jobs: Each country has jobs available

Rich Media:

1. Flag
2. Map

Instances: 40

# 3 Questions: 1

1. What countries are good for people looking for places to work?
2. What countries provide the best quality of life?
3. What countries have the best assistance for the unemployed and immigrants?
